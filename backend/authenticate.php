<?php
require_once(__DIR__ . '/crypto/aes256.php');
require_once(__DIR__ . '/api/message.php');
require_once(__DIR__ . '/api/receive.php');
require_once(__DIR__ . '/api/response.php');

\Api\Receive::read('06c858764a09d6c2c156b6a60f21bc8a', 'uuy6q2uzn47zl5zO1wWj4TWVxogalVxx', function($message)
{
    /** @var \Api\Message $message */
    if(
        (isset($message->data->username) && strcmp($message->data->username, 'Ola') == 0) &&
        (isset($message->data->secret) && strcmp($message->data->secret, '1234') == 0))
    {
        \Api\Response::write(200, (object)['text' => 'Welcome back, Ola'], '06c858764a09d6c2c156b6a60f21bc8a', 'uuy6q2uzn47zl5zO1wWj4TWVxogalVxx');
    } else
    {
        \Api\Response::write(401, null, '06c858764a09d6c2c156b6a60f21bc8a', 'uuy6q2uzn47zl5zO1wWj4TWVxogalVxx');
    }
});