<?php
namespace Crypto
{
    use Exception;

    final class AES256CBC
    {
        /**
         * Create message hash
         *
         * Algorithm: HMAC SHA-256
         *
         * @param string $data
         * @param string $token
         *
         * @return string
         */
        static public function hash($data, $token)
        {
            return hash_hmac('sha256', $data, $token);
        }
        /**
         * Encrypt data with given key
         *
         * Algorithm: AES-256
         * Mode: CBC
         * Padding scheme: PKCS7
         *
         * @param string $key 32 byte (256-bit) key
         * @param string $data Data to encrypt
         *
         * @return string
         * @throws \Exception
         */
        static public function encrypt($key, $data)
        {
            if(gettype($data) != 'string')
                throw new Exception('Encryption content is not string');
            if(($salt = openssl_random_pseudo_bytes(8)) === false)
                throw new Exception('Failed to generate pseudo random bytes');

            $salted = '';
            $dx = '';
            while (strlen($salted) < 48) {
                $dx = md5($dx.$key.$salt, true);
                $salted .= $dx;
            }
            $key = substr($salted, 0, 32);
            $iv  = substr($salted, 32,16);
            if(($encrypted_data = openssl_encrypt($data, 'aes-256-cbc', $key, true, $iv)) === false)
                throw new Exception('Failed to encrypt message content');

            $encrypted = array
            (
                "content" => base64_encode($encrypted_data),
                "salt" => bin2hex($salt),
                "iv" => bin2hex($iv)
            );

            return json_encode($encrypted);
        }

        /**
         * Decrypt data with given key
         *
         * Algorithm: AES-256
         * Mode: CBC
         * Padding scheme: PKCS7
         *
         * @param string $key 32 byte (256-bit) key
         * @param string $data Data to decrypt
         *
         * @return string
         * @throws \Exception
         */
        static public function decrypt($key, $data)
        {
            if (mb_strlen($key, '8bit') !== 32)
                throw new Exception("Needs a 256-bit key!");

            if(($data = json_decode($data, true)) === null)
                throw new Exception('Failed to decode');
            if(!isset($data['salt']))
                throw new Exception('No message salt: ' . print_r($data, true));
            if(!isset($data['iv']))
                throw new Exception('No vector initialization');

            $salt = hex2bin($data["salt"]);
            $iv  = hex2bin($data["iv"]);

            if(($content = base64_decode($data["content"])) === false)
                throw new Exception('Failed to decode message content');
            $concatedPassphrase = $key.$salt;

            $md5 = array();
            $md5[0] = md5($concatedPassphrase, true);
            $result = $md5[0];
            for ($i = 1; $i < 3; $i++)
            {
                $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
                $result .= $md5[$i];
            }
            $key = substr($result, 0, 32);
            if(($decrypted = openssl_decrypt($content, 'aes-256-cbc', $key, true, $iv)) === false)
                throw new Exception('Message content decryption failed');

            return $decrypted;
        }
    }
}