(function(ns)
{
    var _module_test = function()
    {
        /**
         * @alias _module_api_server_api
         * @param {string} name
         * @param {string} path
         * @param {string} method
         * @param {function} callback
         * @public
         */
        var _module_api_server_api = function(name, path, method, callback)
        {
            this.name = name;
            this.path = path;
            this.method = method;
            this.callback = callback;
        };

        /** @type {_module_api_server} */
        this.server = undefined;

        this.event = function(name)
        {
            switch(name)
            {
                case 'deviceReady':
                    this.server = ns.Server({
                        serverBaseUri: 'https://localhost',
                        clientTokenKey: '06c858764a09d6c2c156b6a60f21bc8a',
                        clientEncryptionKey: 'uuy6q2uzn47zl5zO1wWj4TWVxogalVxx'
                    });
                    this.server.send(
                        new _module_api_server_api('client.authenticate', '/authenticate', 'POST', this.serverCallback.bind(this)),
                        {
                            username: 'Ola',
                            secret: '1234'
                        }
                    );
                    break;
            }
        };

        /**
         *
         * @param {_module_api_server_api} api
         * @param {_module_api_server_message} reply
         */
        this.serverCallback = function(api, reply)
        {
            switch(reply.statusCode)
            {
                case 200: // OK
                    console.log('HTTP header status OK');
                    break;
            }
            console.log(reply);
        };
    };

    ns.Test = new _module_test();
})(Module);