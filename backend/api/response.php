<?php
namespace Api
{
    final class Response
    {
        /** @var Message */
        static protected $message;

        static public function write($statusCode, $data, $token, $key)
        {
            http_response_code($statusCode);
            self::$message = new Message();

            switch($statusCode)
            {
                case 200: // OK
                    if(gettype($data) == 'string')
                        self::$message->data = (object)['message' => $data];
                    else
                        self::$message->data = $data;
                    break;
                case 401: // Unauthorized access
                    self::$message->data = (object)['message' => 'Unathorized access'];
                    break;
                default:
                    if($data === null)
                        self::$message->data = (object)['message' => 'Error code ' . $statusCode];
                    else if(gettype($data) == 'string')
                        self::$message->data = (object)['message' => $data];
                    else
                        self::$message->data = $data;
                    break;
            }

            self::$message->encode($token, $key);

            $result = (object)array
            (
                'data' => self::$message->data,
                'checksum' => self::$message->checksum
            );
            print(json_encode($result));
        }
    }
}