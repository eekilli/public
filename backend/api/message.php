<?php
namespace Api
{
    use Crypto\AES256CBC;

    class Message
    {
        /** @var string SHA-256*/
        public $key;
        /** @var string SHA-256 (HMAC) */
        public $checksum;
        /** @var mixed */
        public $data;
        /** @var string */
        public $error;

        protected $algorithm = 'sha256';

        const MAX_DATA_SIZE = 2048;

        /**
         * @param string $message
         * @return boolean
         */
        protected function error($message)
        {
            $this->error = $message;
            return false;
        }
        /**
         * Verify data checksum
         *
         * @param string $token
         *
         * @return boolean
         * @throws \Exception
         */
        protected function verify($token)
        {
            if(gettype($this->data) != 'string')
                return $this->error('Message data is not string value');
            if(gettype($this->checksum) != 'string' || empty($this->checksum))
                return $this->error('Empty message checksum');

            $checksum = AES256CBC::hash($this->data, $token);

            if(strcmp($checksum, $this->checksum) == 0)
                return true;
            return false;
        }

        /**
         * @param string $key
         * @return boolean
         */
        private function encrypt($key)
        {
            try
            {
                if(gettype($this->data) != 'string')
                    return $this->error('Message data is not string value');
                $this->data = AES256CBC::encrypt($key, $this->data);
            }
            catch(\Exception $e)
            {
                return $this->error($e->getMessage());
            }
            return true;
        }

        /**
         * Encode message data
         *
         * @param string $token
         * @param string $key
         *
         * @return boolean
         */
        function encode($token, $key)
        {
            // Encode data with JSON
            if(($this->data = json_encode($this->data, JSON_UNESCAPED_UNICODE)) === false)
                return $this->error('Encoding error');

            // Encrypt data
            if($this->encrypt($key) !== true)
                return false;

            // Encode data to base64
            $this->data = base64_encode($this->data);
            if(empty($this->data))
                return $this->error('Encoding error (2)');

            // Create checksum of data
            $this->checksum = AES256CBC::hash($this->data, $token);
            return true;
        }

        /**
         * @param string $key
         * @return boolean
         */
        private function decrypt($key)
        {
            try
            {
                if(!isset($this->data) || gettype($this->data) != 'string')
                    throw new \Exception('Message data is not string value');
                $this->data = AES256CBC::decrypt($key, $this->data);
            }
            catch(\Exception $e)
            {
                return $this->error($e->getMessage());
            }
            return true;
        }

        /**
         * Decode message data
         *
         * @param string $token
         * @param string $key
         *
         * @return boolean
         */
        function decode($token, $key)
        {
            // Verify message checksum
            if($this->verify($token) !== true)
                return $this->error('Checksum error');

            // Decode base64 data
            if(($this->data = base64_decode($this->data)) === false)
                return $this->error('Decoding error');

            // Decrypt data
            if($this->decrypt($key) !== true)
                return false;

            // Decode JSON data
            if(($this->data = json_decode($this->data)) === null)
                return $this->error('Decoding error (2)');

            return true;
        }
    }
}