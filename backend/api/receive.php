<?php
namespace Api
{
    use Exception;

    final class Receive
    {
        /** @var Message */
        static protected $message;

        static public function read($token, $key, $callback)
        {
            self::$message = new Message();
            // Remember to filter input values
            // checksum = preg_replace(/[^a-z0-9]/), string length must be 64
            self::$message->checksum = $_POST['checksum'];
            // data = preg_replace(/[^A-z0-9+=]/)
            self::$message->data = $_POST['data'];

            if(self::$message->decode($token, $key) !== true)
                throw new Exception(self::$message->error);

            $callback(self::$message);
        }
    }
}