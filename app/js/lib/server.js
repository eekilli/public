"use strict";

var Module =
{
    list: [],

    register: function(name, obj)
    {
        if(this.indexOf(name) > -1)
            throw 'Module with same name already registered: ' + name;
        this.list.push({name: name, obj: obj});
    },
    indexOf: function(name)
    {
        for(var prop in this.list)
        {
            if(!this.list.hasOwnProperty(prop))
                continue;
            if(this.list[prop].name != name)
                continue;
            return prop;
        }
        return -1;
    },
    get: function(name)
    {
        var index = this.indexOf(name);
        if(index == -1)
            throw 'Unknown module: ' + name;
        return this.list[index].obj;
    },
    fire: function(name, data)
    {
        for(var prop in this.list)
        {
            if(!this.list.hasOwnProperty(prop))
                continue;
            if((this.list[prop].obj.hasOwnProperty('event') && typeof this.list[prop].obj.event === 'function')
                || (this.list[prop].obj.__proto__.hasOwnProperty('event') && typeof this.list[prop].obj.__proto__.event === 'function'))
                this.list[prop].obj.event(name, data);
        }
    }
};

/**
 * Whether string is empty or not
 * @param str
 * @returns {boolean}
 */
function isEmpty(str)
{
    return (!str || 0 === str.length);
}

(function(ns)
{
    /**
     * @alias _module_api_server_message_reply
     * @param {number} [statusCode]
     * @param {string} [checksum]
     * @param {*} [data]
     * @public
     */
    var _module_api_server_message_reply = function(statusCode, checksum, data)
    {
        this.statusCode = statusCode ? statusCode : -1;
        this.checksum = checksum ? checksum : '';
        this.data = data ? data : {};
    };
    /**
     * @alias _module_api_aes256cbc
     * @public
     */
    var _module_api_aes256cbc =
    {
        stringify: function(params)
        {
            var data = {
                content: params.ciphertext.toString(CryptoJS.enc.Base64),
                salt: params.salt.toString(),
                iv: params.iv.toString()
            };

            return JSON.stringify(data);
        },
        parse: function(str)
        {
            var data = JSON.parse(str);
            if(!data.hasOwnProperty('content'))
                throw '(_module_api_aes256cbc::parse) No data content';
            if(!data.hasOwnProperty('salt'))
                throw '(_module_api_aes256cbc::parse) No data salt';
            if(!data.hasOwnProperty('iv'))
                throw '(_module_api_aes256cbc::parse) No data initialization vector';

            return CryptoJS.lib.CipherParams.create(
                {
                    ciphertext: CryptoJS.enc.Base64.parse(data.content),
                    salt: CryptoJS.enc.Hex.parse(data.salt),
                    iv: CryptoJS.enc.Hex.parse(data.iv)
                }
            );
        }
    };

    /**
     * @alias _module_api_server_message
     * @param {_module_api_server} server
     * @param {*} [data]
     * @param {string} [checksum]
     * @param {number} [statusCode]
     * @public
     */
    var _module_api_server_message = function(server, data, checksum, statusCode)
    {
        this.data = data ? $.extend({}, data) : {};
        this.statusCode = statusCode ? statusCode : -1;
        this.checksum = checksum ? checksum : '';
        this.error = '';

        this.encrypt = function()
        {
            var result = CryptoJS.AES.encrypt(
                this.data,
                server.options.clientEncryptionKey,
                {
                    format: _module_api_aes256cbc
                }
            );

            this.data = result.toString();
        };
        this.encode = function()
        {
            if(typeof this.data !== 'object' || isEmpty(this.data))
                throw '(_module_api_server_message::encode) Invalid or empty message data';

            this.data = JSON.stringify(this.data);
            this.encrypt();

            this.data = CryptoJS.enc.Utf8.parse(this.data);
            this.data = CryptoJS.enc.Base64.stringify(this.data);

            this.checksum = CryptoJS.HmacSHA256(this.data, server.options.clientTokenKey)
                .toString(CryptoJS.enc.Hex);
        };
        this.parse = function(reply)
        {
            if(!reply.hasOwnProperty('checksum'))
            {
                this.error = 'Invalid message format: no checksum';
                return false;
            }
            if(!reply.hasOwnProperty('data'))
            {
                this.error = 'Invalid message format: no data';
                return false;
            }

            this.checksum = reply.checksum;
            this.data = reply.data;

            return true;
        };

        this.decrypt = function()
        {
            this.data = CryptoJS.enc.Base64.parse(this.data)
                .toString(CryptoJS.enc.Utf8);

            var result = CryptoJS.AES.decrypt(
                this.data,
                server.options.clientEncryptionKey,
                {
                    format: _module_api_aes256cbc
                }
            );

            this.data = result.toString(CryptoJS.enc.Utf8);
        };

        this.decode = function()
        {
            try
            {
                var checksum = CryptoJS.HmacSHA256(this.data, server.options.clientTokenKey)
                    .toString(CryptoJS.enc.Hex);
                if(checksum !== this.checksum)
                {
                    this.error = 'Checksum mismatch';
                    return false;
                }
                this.decrypt();
                this.data = JSON.parse(this.data);
            } catch(error)
            {
                this.error = String(error.message);
                return false;
            }

            return true;
        };
    };

    /**
     * @alias _module_api_server
     * @param opt
     * @public
     */
    var _module_api_server = function(opt)
    {
        this.options =
        {
            serverBaseUri: opt.serverBaseUri,
            clientTokenKey: opt.clientTokenKey,
            clientEncryptionKey: opt.clientEncryptionKey,
            connectionTimeout: (opt.connectionTimeout ? opt.connectionTimeout : 10000)
        };

        /**
         *
         * @param {_module_api_server_api} api
         * @param {object} [data]
         */
        this.send = function(api, data)
        {
            var message = new _module_api_server_message(this, data);
            var reply = new _module_api_server_message_reply();

            message.encode();

            $.ajax({
                type: api.method,
                url: this.options.serverBaseUri + api.path,
                data: {
                    checksum: message.checksum,
                    data: message.data
                },
                dataType: 'json',
                timeout: this.options.connectionTimeout
            })
                .fail((
                    /**
                     * @param {XMLHttpRequest} XMLHttpRequest
                     * @param {string} textStatus
                     */
                    function(XMLHttpRequest, textStatus)
                    {
                        switch(XMLHttpRequest.readyState)
                        {
                            case 4:
                                reply.statusCode = XMLHttpRequest.status;
                                if(reply.statusCode === 200)
                                    reply.statusCode = 500;

                                if(isEmpty(XMLHttpRequest.responseText))
                                {
                                    reply.statusCode = 422;
                                    reply.data.message = 'Invalid server response';
                                    break;
                                }

                                try
                                {
                                    var result = JSON.parse(XMLHttpRequest.responseText);
                                    var response = new _module_api_server_message(this);

                                    if(!response.parse(result))
                                    {
                                        reply.statusCode = 422;
                                        reply.data.message = 'Invalid message: ' + response.error;
                                        break;
                                    }
                                    if(!response.decode())
                                    {
                                        reply.statusCode = 422;
                                        reply.data.message = 'Decryption error: ' + response.error;
                                        break;
                                    }
                                    reply.checksum = response.checksum;
                                    reply.data = response.data;
                                } catch(error)
                                {
                                    reply.statusCode = 422;
                                    reply.data.message = 'Failed to read server response';
                                    break;
                                }
                                break;
                            case 0:
                                console.log(XMLHttpRequest, textStatus);
                                reply.statusCode = 599;
                                reply.data.message = 'Nettverksfeil';
                                break;
                            default:
                                console.log(XMLHttpRequest, textStatus);
                                reply.statusCode = 422;
                                reply.data.message = 'Ukjent feil oppstod';
                                break;
                        }

                        api.callback(api, reply);
                    }).bind(this)
                )
                .done((function(result)
                {
                    var response = new _module_api_server_message(this);
                    if(!response.parse(result))
                    {
                        reply.statusCode = 444;
                        reply.data.message = response.error;

                        api.callback(api, reply);
                        return;
                    }
                    if(!response.decode())
                    {
                        reply.statusCode = 444;
                        reply.data.message = response.error;

                        api.callback(api, reply);
                        return;
                    }

                    reply.statusCode = 200;
                    reply.data = response.data;
                    reply.checksum = response.checksum;

                    api.callback(api, reply);
                }).bind(this));
        };
    };

    /**
     * @param {object} opt
     * @returns {_module_api_server}
     * @constructor
     */
    ns.Server = function(opt)
    {
        return new _module_api_server(opt)
    };


})(Module);